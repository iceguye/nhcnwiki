= 陷阱 =
小心陷阱(traps)！你踩上它的时候，它就会被激活:例如，一块石头(rock)从天花板上掉下来，一块巨石(boulder)滚了出来，一支飞镖(dart)射你一脸，或者一阵闪光亮瞎了你。我们可以通过搜索(search)或者在混乱(confused)时读探测金币卷轴(scroll of gold detection)来探测隐藏的陷阱。

任何一个方格(spuare)内只能存在一个陷阱，如果你不能手动解除(#untrap)物理陷阱，你可以通过进入方格并挖一个坑来移除它们，坑中的陷阱会代替原来的陷阱。你之后就可以避开它，或者可以推一个巨石到坑里去，从而移除这个坑。这种策略的缺点是你必须把自己暴露在陷阱下来移除它。

一些类型的陷阱只能在低于某地牢层深度的拥有正常房间和走廊的地牢层出现[1] 迷宫(Maze) 层被处理的比较特别l,[2] 特殊层和任务(quest)矿坑(mines)fillers是第三种陷阱分布.[3]

很多陷阱可以影响怪物，包括宠物，但不同于它们影响你。一些陷阱甚至不可以被怪物触发。

<<TableOfContents>>

== 反魔法力场 ==

{{attachment:Anti-magic_field.png}}Anti_magic field<<Color2(`^`,lightblue,black)>><<BR>>这个陷阱会使你流失2~你的级别加1点的法力值。如果这使你的法力值为负，你的最大魔法力值也会永久的降低！(4)

魔法抵抗(magic resistance)会防止你的法力流失。("You feel momentarily lethargic")“你一时感到昏昏欲睡”

如果你的最大法力值是零的话反魔法力场会失效。

怪物们不会触发这个陷阱，怪物和陷阱都不会受到影响，并且这个陷阱即使在你的视野中也不会被发现。

产生:可以在任意地牢层出现。

移除:站在陷阱上的时候在地上挖个坑，然后填坑。[5]
== 箭矢陷阱 ==
{{attachment:Arrow_trap.png}} Arrow trap<<Color2("^",cyan,black)>><<BR>>这些箭会射你一脸，通常造成1d6的伤害，或者哪个家伙站在它们上时也是这样。

这些有用的陷阱会提供箭矢资源。当你站在陷阱上或其他相邻的方格，试着使用#untrap命令。也许你会失败，箭会一次次的射到你脸上，但是如果你足够幸运的话，你会得到很多的箭矢 (50 - rnl(50)[6].)你或怪物每一次触发这个陷阱它都会有1/15的几率消失。击中你或怪物的箭矢会消失，你通过解除陷阱获得的箭矢的平均值取决于你的运气，不过它会比你通过在陷阱消失之前站在陷阱上所获得的箭的平均值要高。

产生:可以在任意地牢层随机出现。

解除:#untrap，或者重复的触发它直到它耗尽箭矢("You hear a loud click!")你听到很大的机关声[7]

== 捕兽夹 ==
{{attachment:Bear_trap.png}}Beartrap<<Color2("^",cyan,black)>><<BR>>主要文章来源:[[beartrap]]

捕兽夹会限制你的行动几个回合。在这段时间里，你可以进行攻击或被攻击，但是你不能移动。当你被捕兽夹夹住的时候，你也不能进行踢(kick)的动作。因为一些原因，通过对角线方向移动要比垂直方向移动来逃脱捕兽夹大约要多出五个回合。[8]怪物们也可以触发陷阱。

捕兽夹是可以被解除(untrap)的。如果成功了，就会在地面上出现一个捕兽夹，你可以把它捡起来并在一个新地点使用(apply)它。如果不成功，你就会被夹住。如果一只熊地精(bugbear)或枭熊(owlbear)被捕兽夹困住了且不在你的视野内，你会听见"the roaring of an angry bear".一只熊的怒吼声。如果你被捕兽夹夹住了，你不可以进行解除(#untrap)命令。

另外，捕熊夹可以自己重置。

产生:可以在任意地牢层随机出现。

移除:通过#untrap，或像一个吃金属的动物(metallivore)一样把它吃掉。

== 飞镖陷阱 ==
{{attachment:Dart_trap.png}}  Dart trap<<Color2("^",cyan,black)>>
和箭矢陷阱一样，不过射在你脸上的是飞镖(darts)。飞镖通常会造成1d3的伤害。飞镖可以是淬毒的(1/6的几率)，所以小心些。

解除飞镖陷阱会得到一大堆飞镖，不过缺乏毒抗性的角色尽量不要尝试。像箭矢陷阱一样，它每当被触发时都有1/15的概率消失，击中的飞镖也会消失，你可以通过解除陷阱来获得比触发陷阱更多的飞镖，平均(50 - rnl(50)的飞镖[10])

产生:可以在任一地牢层随机产生。

解除:通过#untrap，或者重复的触发它直到飞镖耗尽 ("You hear a soft click.")你听到轻微的咔嗒声
[11]
== 火焰陷阱 ==
{{attachment:Fire_trap.png}}  Fire trap<<Color2("^",red,black)>><<BR>>这会在你的脚下释放一条火焰柱。如果你没有火焰抗性，你会受到2d4的伤害，并且减小0至你受到伤害大小的最大生命值。如果你具有火焰抗性，你会失去d2-1的生命值(i.e. one point or none), 且不会减小你的最大生命值。陷阱会烧掉一些衣服，五种物品(头盔，torso，盾牌，手套，鞋子)有均等的几率被选择烧毁  If the selected armor is not torso, but is either burnproof, thoroughly burnt, or not worn, a different type of armor will be selected. If blessed, the chance of the selected armor burning is subject to luck consideration. If the final armor selection is not body armor (regardless of whether the armor survived the luck roll), there is a 1/3 chance that scrolls, potions, and spellbooks in the main inventory will not be subject to burning. Otherwise all scrolls, potions, and spellbooks will have a 1/3 chance of burning.
Generation: Fire traps are only randomly generated in Gehennom, maze levels (including to the left and right of the Castle), the Plane of Fire, and some quests, notably the Valkyrie quest. In Gehennom, fire trap generation is increased. 1/5th (20%) of all randomly generated traps in Gehennom are guaranteed to be fire traps.
A magic trap or trapped box or chest has a chance to act as a fire trap even where a fire trap cannot be generated. A magic trap always acts as a fire trap if activated by a monster.
To remove: Dig a pit into the ground while standing on the trap, then fill the pit. In SLASH'EM, it is also possible to disarm a fire trap with a non-cursed potion of water, similar to using a potion of oil on a squeaky board. This will also generate 1-4 potions of oil based on luck. Since oil can then be diluted back to water, the large number of fire traps in Gehennom offer an effective way of multiplying one's water potions.

== 洞 ==
{{attachment:Hole.png}} Hole <<Color2("^",brown,black)>>主要文章来源:Hole<<BR>>洞会直接让你掉落几层。一般会掉落2-3层，掉落层数没有上限，它取决于你能掉落的层数，尽管你不会掉落到城堡(the castle)以下。怪物们永远不会掉落超过一层[13]

解决这个问题的一种方法是走下你所看发现的每一个楼梯，然后再走上来。这样的话，如果你掉落到那一层你就会找到出口的位置。具有高的灵活度(dexterity)会减少你从坑(pit)里掉下去或触发陷阱门(trapdoor)的几率。

一个相同的洞也不能保证通向同一个位置，甚至是同一层，即使是你紧跟着一个对象(你的宠物或怪物)通过它的时候也不行。

产生:洞比其他类型随机出现的要更少。7次产生洞的尝试只有一次会成功。洞是不能在不可挖掘的地牢层，像振动方块(the Vibrating square )层或城堡随机产生的。如果所在地牢层不可挖掘，洞会被落石陷阱代替。洞是不能在一块巨石下产生的。

移除:推一块巨石进去。[14]
== 魔法陷阱 ==
{{attachment:Magic_trap.png}}   
Magic-trap<<Color2("^",brightbule,black)>>主要文章来源:Magic trap<<BR>>魔法陷阱的效果是多种多样的。。。

怪物会有20/21的几率触发失败，这种情况下陷阱没有效果且不会被你发现。如果怪物触发了这个陷阱，它会像火焰陷阱一样释放出一条火焰柱。

产生:可以在任意地牢层随机产生。

移除:重复的触发它直到它爆炸。
[15]
== Falling-rock-trap ==
{{attachment:Falling_rock_trap.png}}  <<Color2("^",lightgray,black)>>

== Land-mine ==
{{attachment:Land_mine.png}}  <<Color2("^",red,black)>>

== Level-teleporter ==
{{attachment:Level_teleporter.png}}   <<Color2("^",red,black)>>

== Pit ==
{{attachment:Pit.png}}  <<Color2("^",darkgray,black)>>

== Polymorph-trap ==
{{attachment:Polymorph_trap.png}}  <<Color2("^",brightgreen,black)>>

== Magic-portal ==
{{attachment:Magic_portal.png}} <<Color2("^",brightmagenta,black)>>

== Rolling-boulder-trap ==
{{attachment:Rolling_boulder_trap.png}} <<Color2("^",lightgray,black)>>

== Rust-trap ==
{{attachment:Rust_trap.png}}  <<Color2("^",blue,black)>>
