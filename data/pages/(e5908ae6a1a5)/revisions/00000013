= 吊桥 =
NetHack中共有三个吊桥——一个在[[城堡]]([[Castle]])，还有两个在[[女武神的任务]]([[Valkyrie quest]])地图。它们一般被认为是极其危险的;然而有经验的玩家则会小心的设法利用它的'''恐怖'''力量，使之变成有用的工具。关闭的吊桥使用<<Color2(`#`,brown,black)>>表示，打开的吊桥使用<<Color2(`.`,brown,black)>>表示。


<<TableOfContents>>

== 打开和关闭 ==

位于[[城堡]]的吊桥可以通过演奏'''[[可奏乐的乐器]]'''的特定[[音符]]([[passtune]])来打开。那个音符可以在[[祭坛]]上祈祷由你的神指示你来获得，也可以通过在吊桥前玩音乐游戏推理出来:齿轮声(gears) = 正确的音符，正确的位置;开关声(tumblers) = 正确的音符，错误的位置。注意此时[[声音]]设置必须是开启的。

奇怪的是，用这种方式开关吊桥不计入回合数，这是游戏漏洞[[C343-318]]。

吊桥也可以通过使用[[开锁魔杖]]，开锁术或[[被祝福的]][[开启之铃]]([[Bell of Opening]])打开，并通过使用上锁魔杖或上锁术关闭。

当吊桥关闭的时候，站在它上面或在它即将落下的格子(吊闸)上的怪物(或者玩家)将被杀死并同时摧毁留下来的财产。能进行[[相位移动]]<<FootNote(这个相位移动“phasing”在游戏里就是可以穿墙，穿越固体的意思。译成相位移动因为约定俗成，phase的本意是相位。dnd系怪物有索尔石怪幽灵之类，dota里面有相位鞋，美漫里也有穿墙术这样的超能力，总之就是穿越地形。 __Msajka)>>(phasing)的怪物不会被杀死(除非在那两个格子上都有相位移动怪物，其中一个会无处可去而被杀死。)如果正处于飞行或漂浮状态将有机会生存， 不过如果站在吊闸上就会--尽管试试吧，你会收获教训的。
当吊桥打开的时候，困在其之下的怪物会遭受同样的劫难。

使用吊桥杀死[[和平的]]怪物有50%的几率减少你的[[运气]]，因为你事先没有激怒它。

{{attachment:Drawbridge1.JPG}}

''城堡吊桥的危险区(红色的#)和控制区(绿色的C)''

== 毁坏 ==

如果吊桥被冲击波(force bolt)击中就会毁坏。在“打开”格子和“关闭”格子(吊闸或护城河)的非相位移动怪物几乎都会被杀死。玩家被杀死的几率与击打点数(hit points)和装甲等级(armor class)无关，而飞行和悬浮状态会救你一命。

只有上述两个格子是危险的。不过传说爆炸迸溅出来的金属碎片会在很远的距离外击中你。

一旦吊桥被毁坏就不会再重新产生。向吊闸的格子施用巫师锁法术或挥动上锁魔杖只会生成一扇普通的门。

== 策略 ==

只要你得知了音符，吊桥就可以通过演奏乐器来开关。所有站在护城河和吊闸格子上的生物都会被杀死(你会获得击杀经验)。如果飞行的怪物碰巧的躲过一劫，就再来一次，此时游戏时间不会流逝。这是清除城堡周边怪物群的极其安全的一种方法。不过这种方法也会摧毁怪物身上携带的财产。(这个建议只对原版NetHack 3.4.3有效;在大多数NetHack变体中此漏洞(C343-318)已被修复。


If you stand in a line with the drawbridge and the castle fountain then soldiers inside may try to zap you with wands of striking, destroying the drawbridge. For this reason you should stand a knight's move away from the drawbridge when using it to crush the inhabitants.
Exercise caution when crossing the drawbridge. You never know when a monster with a wand of striking may be lurking. Some players prefer to destroy the bridge when they're done crushing things. Alternatively, jumping (other than the Knight's innate L-jump) can take you through the danger zone in a single move, leaving no time for monsters to destroy the drawbridge under you. Player deaths from the drawbridge are relatively rare, but are particularly feared nonetheless. Likely this is because it is the only non-delayed instadeath for which the immediate cause is outside the player's direct control (a monster zapping a wand of striking), and for which no common resistance provides any protection.
