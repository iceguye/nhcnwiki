= 陷阱 =
注意[[陷阱]]！站在它上面会激活它:有时候'''石头'''从天上掉下来，一块'''巨石'''滚出来，'''飞镖'''射你脸上，或者一阵闪光让你'''目盲'''。可以通过'''搜索'''或者在混乱时读'''探测金币卷轴'''来探测隐藏的陷阱。

Beware of '''traps'''! When you step on one, it activates: for example, a '''rock''' falls from the ceiling, a '''boulder''' is released, a '''dart''' is fired, or a flash of light '''blinds''' you.To detect hidden traps, '''search''' for them or read '''a scroll of gold detection''' while confused

.
一个方格(spuare)内只能存在一个陷阱，如果你不能解除(#untrap)物理陷阱，你可以在陷阱上挖一个坑，坑中的陷阱会代替原来的陷阱。你可以避开它，或者推一个巨石到坑里去，从而移除这个坑。但你必须暴露在陷阱下去移除它。

一些类型的陷阱只能在低于某深度的拥有房间和走廊的地牢层出现[1] 迷宫(Maze) 层被处理的比较特别l,[2] 特殊层和任务(quest)矿坑(mines)fillers是第三种陷阱分布.[3]

很多陷阱可以影响到怪物，包括宠物，但效果和你不一样。而一些陷阱不可以被怪物触发。

<<TableOfContents>>
== 陷阱类别 ==
=== 反魔法力场 ===

{{attachment:Anti-magic_field.png}}Anti_magic field<<Color2(`^`,#5555ff,black)>><<BR>>这个陷阱会使你流失2~你的级别加1点的法力值。如果这使你的法力值为负，你的最大法力值也会永久的降低！(4)

魔法抵抗(magic resistance)会防止你的法力流失。("You feel momentarily lethargic")“你一时感到昏昏欲睡”

如果你的最大法力值是零的话反魔法力场会失效。

怪物们不会触发这个陷阱，并且这时陷阱即使在你的视野中也不会被发现。

产生:可以在任意地牢层出现。

移除:站在陷阱上的时候在地上挖个坑，然后填坑。[5]
=== 箭矢陷阱 ===
{{attachment:Arrow_trap.png}} Arrow trap<<Color2(`^`,cyan,black)>><<BR>>这些箭会射你一脸，造成1d6的伤害，其他哪个家伙站在它们上时也是这样。

这些陷阱可以提供箭矢。当你站在陷阱上或相邻的方格，使用#untrap命令。如果失败了，箭就会射到你脸上，但是幸运的话你会得到很多的箭矢 (50 - rnl(50)[6].)你或怪物每一次触发这个陷阱都会有1/15的几率消失。击中你或怪物的箭矢会消失，解除陷阱获得的箭矢的平均值取决于你的运气，不过它会比你站在陷阱上所获得的箭的平均值要高。

产生:可以在任意地牢层随机出现。

解除:#untrap，或者重复的触发它直到它耗尽箭矢("You hear a loud click!")你听到很大的咔嗒声[7]

=== 捕兽夹 ===
{{attachment:Bear_trap.png}}Beartrap<<Color2(`^`,cyan,black)>><<BR>>主要文章来源:[[beartrap]]

捕兽夹会限制你的行动几个回合。在这段时间里，你可以进行攻击或被攻击，但是你不能移动。当你被捕兽夹夹住的时候，你也不能进行踢(kick)的动作。因为某些原因，通过对角线方向移动要比垂直方向移动来逃脱捕兽夹大约要多出五个回合。[8]怪物们也可以触发陷阱。

捕兽夹是可以被解除(untrap)的。如果成功就会在地面上出现一个捕兽夹，你可以把它捡起来并在一个新地点使用(apply)它。如果不成功你就会被夹住。如果一只熊地精(bugbear)或枭熊(owlbear)被捕兽夹困住了且不在你的视野内，你会听见"the roaring of an angry bear".一只熊的怒吼声。如果你被捕兽夹夹住了，你不可以进行解除(#untrap)命令。

另外，捕熊夹可以自己重置。

产生:可以在任意地牢层随机出现。

移除:通过#untrap，或像一个吃金属的动物(metallivore)一样把它吃掉。

=== 飞镖陷阱 ==
{{attachment:Dart_trap.png}}  Dart trap<<Color2(`^`,cyan,black)>>
它和箭矢陷阱一样，不过射在你脸上的是飞镖(darts)。飞镖通常会造成1d3的伤害。飞镖可以是淬毒的(1/6的几率)，所以小心些。

解除飞镖陷阱会得到一大堆飞镖，不过缺乏毒抗性的角色尽量不要尝试。像箭矢陷阱一样，它每当被触发时都有1/15的概率消失，击中的飞镖也会消失，解除陷阱会获得比触发陷阱更多的飞镖，平均(50 - rnl(50)的飞镖[10])

产生:可以在任一地牢层随机产生。

解除:通过#untrap，或者重复的触发它直到飞镖耗尽 ("You hear a soft click.")你听到轻微的咔嗒声
[11]
=== 火焰陷阱 ===
{{attachment:Fire_trap.png}}  Fire trap<<Color2("^",red,black)>><<BR>>这会在你的脚下释放一条火焰柱。如果你没有火焰抗性，你会受到2d4的伤害，并且减小0至你受到伤害大小的最大生命值。如果你具有火焰抗性，你会失去d2-1的生命值(i.e. 一点或没有), 且不会减小你的最大生命值。陷阱会烧掉一些衣服，五种物品(头盔，躯干，盾牌，手套，鞋子)有均等的几率被选择烧毁  如果被选择的不是躯干，同时是防火的或彻底烧毁或没有穿的，另一种类的装甲会被选择。被选择的装甲是祝福的，它燃起来的几率会考虑运气。如果最终选择的不是躯体装甲(无论它是否因为运气而得以保存)会有1/3的几率使主物品栏的卷轴，药剂和魔法书免于烧毁。否则所有的卷轴，药剂和魔法书会有1/3的几率被燃烧。

产生:火焰陷阱只随机产生于Gehennom, 迷宫层 (包括the Castle的左边和右边), the Plane of Fire, 和一些 quests, 尤其是女武神的 quest.在Gehennom,火焰陷阱的产生几率会增大。所有在Gehennom随机产生的陷阱有1/5th (20%) 是火焰陷阱。魔法陷阱，陷阱盒或箱子有几率和火焰陷阱同效，即使在火焰陷阱不能被产生的地方。魔法陷阱被怪物激活时和火焰陷阱一样

解除:站在陷阱上的时候在地上挖一个坑，然后填坑。在SLASH'EM中，也可以用未被诅咒的水来解除它，就和在吱吱作响的板子(squeaky board)上用油一样。这么做还会取决于运气产生1-4瓶的油。由于油可以稀释成水，在地狱(Gehennom)中大量的火焰陷阱会提供一种复制水的途径。

=== 洞 ===
{{attachment:Hole.png}} Hole <<Color2("^",brown,black)>>主要文章来源:Hole<<BR>>洞会直接让你掉落。一般会掉落2-3层，掉落层数没有上限，它取决于你能掉落的层数，尽管你不会掉落到城堡(the castle)以下。怪物们永远不会掉落超过一层[13]

解决这个问题的一种方法是走下你所看发现的每一个楼梯，然后再走上来。这样的话，如果你掉落到那一层你就会找到出口的位置。具有高的灵活度(dexterity)会减少你从坑(pit)里掉下去或触发陷阱门(trapdoor)的几率。

一个相同的洞也不能保证通向同一个位置，甚至是同一层，即使是你紧跟着一个对象(你的宠物或怪物)也不行。

产生:洞比其他类型随机出现的要更少。7次产生洞的尝试只有一次会成功。洞是不能在不可挖掘的地牢层--像振动方块(the Vibrating square )层或城堡随机产生的。如果所在地牢层不可挖掘，洞会被落石陷阱代替。洞是不能在一块巨石下产生的。

移除:推一块巨石进去。[14]
=== 魔法陷阱 ===
{{attachment:Magic_trap.png}}   
Magic-trap<<Color2(`^`,#5555ff,black)>>主要文章来源:Magic trap<<BR>>魔法陷阱的效果是多种多样的。。。

怪物会有20/21的几率触发失败，这种情况下陷阱没有效果且不会被你发现。如果怪物触发了这个陷阱，它会像火焰陷阱一样释放出一条火焰柱。

产生:可以在任意地牢层随机产生。

移除:重复的触发它直到它爆炸。
[15]
=== 落石陷阱 ===
{{attachment:Falling_rock_trap.png}} Falling_rock_trap<<Color2(`^`,lightgray,black)>><<BR>>它会触发石头掉落，对你造成2d6的伤害，除非你正戴着一种硬的帽子，它会把你受到的伤害减小到2。掉落的石头会留在地板上，所以这个陷阱也可以作提供石头的资源，尽管陷阱最终会被耗尽(1/15的几率每次触发)。

早期的宠物死亡常常是由于这种陷阱，所以它有着 "Puppy Pounder" 和 "Kitty Killer"的外号。

产生:可以在任一地牢层随机产生。

解除:重复的触发它直到耗尽石头。[16]
A trapdoor in the ceiling opens and a rock falls on your head!
A trapdoor in the sky opens and a rock falls on your head! (outdoors, e.g. in the Home level of some Quests.)
=== 地雷 ===
{{attachment:Land_mine.png}}  Land Mine<<Color2(`^`,red,black)>>主要文章来源: 地雷<<BR＞＞地雷是隐藏的爆炸物，在the Fort Ludios很常见. 当它被触发时，它会炸伤你的腿并在爆炸后出现一个坑。你可以通过#untrap在相邻的方格来解除地雷，然后便可以把他捡起来并安装在其他的位置，就像捕兽夹一样。

怪物们有2/3的几率触发失败此陷阱，剩下的几率效果和你一样。

产生:从不在地牢第六层之上随机产生; the main treasure vault in Fort Ludios is often full of them.

解除:通过#untrap, 或把它触发(尽管之后它会被坑代替)
[17]


=== 他层传送门 ===
{{attachment:Level_teleporter.png}}  Level_teleporter<<Color2(`^`,red,black)>>

它会把你传送到一个随机层的随机位置。如果你有传送控制能力，并且不处在眩晕，晕倒，冰冻，混乱(有1/5的几率)，你就可以指定你(和相邻的/受约束的宠物)想去的地牢层。否则，你和你的怪物会被随机传送，最多上升至第一层，最多下降三层，不过永远不会去主地牢之外或停留在当前地牢层 或在执行仪式之前进入Sanctum. 在Endgame, 你，或持有岩德护身符的怪物和元素们在他们的位面是不可以进行他层传送的。 All eligible target levels are equally likely.[18]
在一次成功的(或不成功的)传送之后它就会消失。

魔法抵抗会阻止你(而不是怪物)的传送
("You feel a wrenching sensation.")
产生:从不在第五层以上或传送禁止的层出现。

移除:独自触发它;怪物触发它时它不会被移除。

[19]
=== 坑 ===
{{attachment:Pit.png}}pit  <<Color2(`^`,darkgray,black)>>
文章主要来源: 坑

坑会在你踩上它们的时候出现，造成1~6的伤害。他也会减少力量和灵敏度。他们需要3~7回合来“爬到边缘”并逃离。你可以用#jump指令或(a)鞭子(whip)来逃脱。在坑里的时候，你所持的任何光源都不会照亮坑壁以外的地方。当作为一个恶魔或蝮蛇掉进坑里会产生YAFM。


产生:可以在任意地牢层随机产生。不能在巨石之下产生。

移除:推一块巨石进去。[20]


=== 变形陷阱 ===
{{attachment:Polymorph_trap.png}} Polymorph trap<<Color2("^",brightgreen,black)>>
主要文章来源: 变形陷阱

变形陷阱会在有限的时间里把你变成其他的怪物。怪物也可以使用这个陷阱。不过，被变形的怪物(包括宠物)会永久保持那种形态。在一次成功的将玩家变形之后它会消失。

魔法抵抗会阻止你(和你正骑着的座骑)变形. ("You feel momentarily different.")

产生:不在第八层以上出现。

移除:独自触发它;当宠物或怪物触发它时不会被移除[21]

=== 魔法传送阵 ===
{{attachment:Magic_portal.png}}Magic_portal<<Color2(`^`,brightmagenta,black)>>
它是用^符号来表示，因此它也是一种陷阱。它们被用来把你在各地牢分支和元素层之间传送。临时传送门可被the Eye of the Aethiopica产生。

它不像其他传送陷阱，即使你有魔法抵抗它也会有效。它们的目的地是固定的，不受传送控制影响。

产生:不随机产生。

移除:不可被移除，虽然你任务的传送门会因你被拒绝而消失
[22]

百科全书条目
{{{
Portals can be Mirrors, Pictures, Standing Stones, Stone
Circles, Windows, and special gates set up for the purpose.
You will travel through them both to distant parts of the
continent and to and from our own world. The precise manner
of their working is a Management secret.
[ The Tough Guide to Fantasyland, by Diana Wynne Jones ]
}}}

=== 滚石陷阱 ===
{{attachment:Rolling_boulder_trap.png}}Rolling-boulder-trap <<Color2(`^`,lightgray,black)>>
这是一个由两部分组成的陷阱。实际运作的陷阱是一个开关，会在你站在上面的时候激活，使一块附近的巨石向你滚过来。

这些陷阱的存在可以通过在房间里→不协调←的巨石推断。在巨石和陷阱之间的路径必须无碍，尽管巨石可以通过一些地牢地形类似泉水。


当它撞到另一块巨石时“你听到一声巨响，就像一块大石头撞上另一个”，它会停下，而另一块巨石开始移动。"you hear a loud crash as a boulder sets another in motion"

产生:从不在地牢第二层以上产生。


移除:破坏巨石、把它移到陷阱后面或使它与陷阱不在一条直线上，然后激活陷阱;滚石陷阱只会在有巨石存在于两个指定的启动格子之一时被激活，激活陷阱时，且周围没有合适的巨石，陷阱会毁坏


=== 生锈陷阱 ===
{{attachment:Rust_trap.png}}Rust trap  <<Color2(`^`,blue,black)>>

这个陷阱会向你喷水，使水射中的你正穿着的任意一件可生锈的物品生锈。你正手持着的铁质物品或卷轴也会受到影响，但魔法书和药水不受影响。Gremlins喜欢用这些陷阱来繁殖。铁魔像会被这种陷阱彻底毁坏。如果你变形成铁魔像然后你触发了陷阱，你会受到等同于你怪物形态的最大生命值的伤害，杀死你的怪物形态回归你的本来面目。物理伤害减半的外在能力可以生效. 

产生:可以在任意地牢层随机产生。

移除:当站在陷阱上时向下挖一个坑，然后填坑。在SLASH'EM中，可以使用#untrap指令，如果成功了，陷阱会变成一个泉水。

=== 睡眠气体陷阱 ===

{{attachment:Sleeping_gas_trap.png}}Sleeping gas trap<<Color2(`^`,Brightblue,black)>>

=== 钉刺坑 ===

{{attachment:Spiked_pit.png}}Spiked pit<<Color2(`^`,Darkgray,black)>>

=== 吱吱响的板 ===

{{attachment:Squeaky_board.png}}Squeaky board<<Color2(`^`,Brown,black)>>

=== 雕像陷阱 ===

{{attachment:Statue_trap.png}}Statue trap<<Color2(`^`,white,black)>>

=== 传送陷阱 ===

{{attachment:Teleportation_trap.png}}Teleportation trap<<Color2(`^`,Magenta,black)>>

=== 暗门 ===

{{attachment:Trap_door.png}}Trap door<<Color2(`^`,Brown,black)>>

=== 蛛网 ===

{{attachment:Web.png}}Web<<Color2(`^`,Lightgray,black)>>

=== Cold trap ===

=== Container trap ===

=== Door trap ===

== 策略 ==
