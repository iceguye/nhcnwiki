'''鉴定'''(Identification)被认作是NetHack游戏的核心之一。大多数的物品在游戏开始时都是未被鉴定的，一般只描述了它的外观。游戏中有很多方法来鉴定它们。

<<TableOfContents>>

= 鉴定的机制 =
NetHack中有三个鉴定的“级别”：

 *自动鉴定

像正常所认知的鉴定一样，自动鉴定使物品的名字为其原本的名字，而不论它们的外观和你称呼它的名字。

 *提示鉴定

在一些情况下，你会被提示输入你刚刚使用的物品的名字。比如说怪物投掷的药水镶在了你的脑袋上，药水是不会被自动鉴定的。由怪物使用的产生不同效果的药水也会这样，比如说被诅咒的升级药水。把戒指扔进水槽有时也会产生这样的提示。

 *无提示鉴定

在一些情况下，有些物品效果可以使得翻看攻略或十分有经验的玩家可以凭经验鉴定他们。这在用魔杖刻写或用价格鉴定时很常见。你应该使用#name指令来为那些发生特定效果的物品起一个恰当的名字。

有时执行动作会消耗掉物品，此时你应该先命名那个将要消失的物品，然后当你再次得到它时则重新命名。一个常见的例子就是向独角兽(unicorn)投掷宝石(gems)：独角兽会“感激的”(gratefully)接受有价值的宝石，“彬彬有礼的”(graciously)接受玻璃。先命名以区分宝石意味着你再找到相同宝石时就知道了它的价值。如果独角兽只是彬彬有礼的接受了宝石，就重命名它为玻璃。把物品浸入(#dip)到药水里来鉴定也是同样的方法。

= 直接鉴定 =

这些方法不基于物品的特殊属性，都是通用方法，并且所有的结果由游戏引擎来命名（不需要玩家命名）。




