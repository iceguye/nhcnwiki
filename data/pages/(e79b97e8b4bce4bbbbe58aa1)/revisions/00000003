地图暂时先上传截图。文字颜色的功能不是所见即所得，且预览的时候格式会不一样，所以编写起地图会需要一定的时间。

The [[Rogue]] quest sees you fighting the [[Master Assassin]] for [[The Master Key of Thievery]]. For more information on the quest [[branch]] in general, see the [[quest]] article.
<<TableOfContents>>
== Levels ==

Random monsters on this Quest are generated with the following frequencies:
*96/175	(55%)	[[leprechaun]]
*24/175	(14%)	random [[nymph|n]]
*24/175	(14%)	[[guardian naga]]
*6/175	(3%)	random [[naga|N]]
*1/7	(14%)	normal random monster

=== The Thieves' Guild Hall ===
{{{
x={{msl|Master of Thieves}}

--------------------------------->-------------------------------- ---------
|.....|.-|..........|....|......|.|.........|.......+............|-|.......|
|.....|..+..........+....---....S.|...-S----|.-----.|............+.+.......|
|.....+.-|........---......|....|.|...|.....|.|...|.---.....------.--------|
|-----|.-------|..|........------.----|.....|.--..|...-------..............|
|.....|........------+------..........+.....|..|-S---.........------.-----.>
|.....|.------...............-----.}}.--------.|....-------.---....|.+...---
|..-+--.|....|-----.--------.|...|.....+.....|.|....|.....+.+......|.--....|
|..|....|....|....+.|......|.|...-----.|.....|.--...|.....|.|......|..|....|
|..|.-----S----...|^+....-----...|...|.----..|..|.---....--.---S-----.|----|
|..|.|........|...|-----.|.S.....|...|....|----.+.|......|..|.......|.|....|
----.-------..|...|....|.|.|.....|..x----.|...|.|---.....|.|-.......|.---..|
>..........|..S...|....|--.----S----..|...|...+.|..-------.---+-....|...--+|
----------.---------...|......|....S..|.--|...|.|..|...........----.---....|
|........|.........|...+.------....|---.--|...|.--+-.----.----....|.+...--+|
|........|.---+---.-----.-|........|......-----......|..|..|.--+-.|.|S-.|..|
|........|.|.....|........----------.----.......---.--..|-.|....|.-----.|..|
|----....+.|.....----+---............|..---------.+.|...SS.|....|.......|..|
|...--+-----.....|......|.-----------|............|--...||.------+--+---|..|
|..........S.....|......|.|..........S............|.....||...|.....|....|..|
------------------------->--------------------------------------------------
}}}

The two-way [[magic portal]] back to the Dungeons of Doom is at the marked point. The [[Master of Thieves]] is at the point marked 'x', where there is also a [[chest]]; two [[thug]]s are in the room with him, and three and four more in the two rooms immediately connecting to his. Three of the four marked downstairs are actually [[mimic]]s; each of the four also has a water nymph and leprechaun next to it, and there are five more water nymphs and four more leprechauns distributed fairly evenly about the streets; there are also eight [[chameleon]]s on the level, and sixteen random [[trap]]s.
The entire level is [[no-teleport]] and has [[undiggable]] walls and floor.

=== Upper filler level ===

This is an "ordinary" room-and-corridor level, with six rooms; five leprechauns, two water nymphs, and two guardian nagas; nine random objects; and eight random traps.

=== The Assassins' Guild Hall ===
{{{
?=[[Scroll of teleportation|{{white|?}}]]

             ---------------------------------------------------     -------
           ---.................................................|     |.....|
         ---...--------........-------.......................---     ---...|
       ---.....|      ---......|     ---..................----         |-.--
     ---.....---        --------       --..................--         --..| 
   ---...-----                       ---|.----.....----.....---      --..-- 
----..----                       -----..|--  |...---  |.......---   --...|  
|...---                       ----....---    |.---    |.........-- --...--  
|...|                      ----.....---      ---      |..........---....|   
|...----                ----......---                 |...|.......|....--   
|......-----          ---.........|               -----...|............|    
|..........-----   ----...........---       -------......-|...........--    
|..............-----................---     |............|--..........|     
|------...............................---   |...........-- |.........--     
|.....|..............------.............-----..........--  --........|      
|.....|.............--    ---.........................--    |.......--      
|.....|.............|       ---.....................---     --......|       
|-S----------.......----      |-.................----        |.....--       
|..........?|..........--------..............-----           --....|        
|...........|............................-----                |....|        
------------------------------------------                    ------        
}}}

The stairs are located randomly on the level, as are eighteen leprechauns, six guardian nagas, five chameleons, and three random N; six random traps; and fifteen random objects. There is a cursed [[scroll of teleportation]] at the marked spot in the lower left.
The entire level has undiggable walls. Teleportation is permitted.

=== Lower filler level(s) ===

These are "ordinary" room-and-corridor levels, with six rooms; five leprechauns, two water nymphs, and two guardian nagas; nine random objects; and eight random traps.

=== Stronghold of the Master Assassin ===
{{{
a={{red|&#124;}}
b={{red|-}}
x={{msl|Master Assassin}}
%=[[tin|{{cyan|%}}]]

 -----      -------.......................................-------------------
 |...|  ----|.....a.......................................|.................|
 |...|---...|.....a.......................................|....---------....|
 |.---......---..bb.................................-----------|.......|....|
 |...............a..................................|..|...|...----........--
 |.....-----....bb.................................--..--..--.....----S----| 
 |--S---...|....a.................................--........--....|........| 
 |.........-----bbbb.............................--....}}....--...|...|....| 
 |....|.....S......a............................--.....}}.....--..--.------| 
 |-----.....--.....a...........................--...}}}}}}}}...--....|.....--
 |...........--....bbbbbbS-----........x......--....}}}}}}}}....--..........|
 |............--........a...| |..............--.....}}.}}........----------S|
 |.............|........a..%| |..............|......}}}}}}}}......|...|.....|
 |S-.---.---.--|.---.---a...| |-----------...--........}}.}}.....-|..---....|
 |.--|.---.--|.---.-S-..a---| |....|.....|....--....}}}}}}}}....--|..S.---..|
 |...|.......|..........a...---....---...S.....|-...}}}}}}}}...--.S..|...|..|
 |...|..|....|..........a............|..-|..------.....}}.....--..|--|...|S-|
 |...|---....|---.......abbbbb ......|...|--|    --....}}....--...|..--.--..|
 -----.....---.....--.---....bb...--------..|     --........--....|.........|
     |.............|..........a.............S...   -S-------|.....|..-----..|
     ----------------------------------------  ......       ----------   ----
}}}

The stairway into the level is randomly placed in one of the rooms on the left side of the level. The [[Master Assassin]], with the [[Bell of Opening]] and the [[Master Key of Thievery]], is at the point marked {{msl|Master Assassin}}. The $-shaped pool is stocked with four [[shark]]s. There is a [[tin]] of chameleon meat at the marked spot {{cyan|%}} in a left-central room. Randomly placed around the level are eighteen leprechauns, eight guardian nagas, five chameleons, and three random N; thirteen random objects; and eleven random traps, in addition to the one marked spiked pit.

The entire level is no-teleport and has undiggable walls. Note that this level has four separate, unconnected sections.  The stairs down to the level are in the leftmost section, whose boundary is marked in red.  This area is cut off from the Master Assassin; you can only reach the quest nemesis and successfully complete this quest by either [[phasing]] through these walls, or [[hole|falling]] from the level above and hoping that you'll randomly fall into the correct section.  The other two sections are the $-shaped pool and the rightmost section.  If you fall, make sure you have a means of return in case it's necessary to try repeatedly!  You have about a one in four chance falling into the correct section, and even if you are successful in entering it, you will need a way to [[level teleport]] or [[phasing|phase]] out of that section.

Also, a [[drum of earthquake]] can summon the nemesis. ([http://tasvideos.org/GameResources/DOS/Nethack.html Exploiting] [[bug#C343-426|bug C343-426]] could get you across the wall, too.)

Sometimes, a [[magic whistle]] can be used to bring a sufficiently high level pet into the area. It takes some time for the pet to wander near the Master Assassin, but will eventually happen. (After the pet is sent in, stand at the bottom right of the accessible area until it engages the Master Assassin.)

This is a deliberate and acknowledged design decision; Rogues are expected to be resourceful!

== Messages ==
=== Entry ===
First time: 
 Unexpectedly, you find yourself back in Ransmannsby, where you trained to
 be a thief.  Quickly you make the guild sign, hoping that you AND word
 of your arrival reach the Master of Thieves' den.

Next time:
 Once again, you find yourself back in Ransmannsby.  Fond memories are
 replaced by fear, knowing that the Master of Thieves is waiting for you.

If already rejected twice due to bad alignment:
 You rub your hands through your hair, hoping that the little ones on
 the back of your neck stay down, and prepare yourself for your meeting
 with the Master of Thieves.

=== Quest guardians ===

If #chatting before the quest is complete:
 "I hear that Lady Tyvefelle's household is lightly guarded."
 "You're back?  Even the Twain don't come back anymore."
 "Can you spare an old cutpurse a zorkmid for some grog?"
 "Fritz tried to join the other side, and now he's hell-hound chow."
 "Be careful what you steal, I hear the boss has perfected turning
 rocks into worthless pieces of glass."

If #chatting after the quest is complete:
 "I was sure wrong about Lady Tyvefelle's house; I barely got away with my
 life and lost my lock pick in the process."
 "You're back?  Even the Twain don't come back anymore."
 "Can you spare an old cutpurse a zorkmid for some grog?"
 "Fritz tried to join the other side, and now he's hell-hound chow."
 "Be careful what you steal, I hear the boss has perfected turning
 rocks into worthless pieces of glass."

=== Quest leader ===

When you first meet your quest leader:
 "Well, look who it is boys -- <playername> has come home.  You seem to have
 fallen behind in your dues.  I should kill you as an example to these
 other worthless cutpurses, but I have a better plan.  If you are ready
 maybe you could work off your back dues by performing a little job for
 me.  Let us just see if you are ready..."

When you return, having been rejected due to lack of experience:
 "Well, I didn't expect to see you back.  It shows that you are either stupid,
 or you are finally ready to accept my offer.  Let us hope for your sake it
 isn't stupidity that brings you back."

This message is not currently used:
 "Did you perhaps mistake me for some other Master of Thieves?  You must
 think me as stupid as your behavior.  I warn you not to try my patience."

When you are expelled from the quest for having failed the alignment test
seven times:
 "Well thugs, it looks like our friend has forgotten who is the boss
 around here.  Our friend seems to think that <currentranks> have been put in
 charge.  Wrong.  DEAD WRONG!"
 Your sudden shift in surroundings prevents you from hearing the end
 of the Master of Thieves' curse.

When being rejected due to lack of experience:
 "In the time that you've been gone you've only been able to master the
 arts of a <currentrank>?  I've trained ten times again as many Robbers
 in that time.  Maybe I should send one of them, no?  Where would that
 leave you, <playername>?  Oh yeah, I remember, I was going to kill you!"

When being rejected due to having worse than pious alignment:
 "Maybe I should chain you to my perch here for a while.  Perhaps watching
 real chaotic men at work will bring some sense back to you.  I don't
 think I could stand the sight of you for that long though.  Come back
 when you can be trusted to act properly."

When finally assigned the quest:
 "Will everyone not going to retrieve the Master Key of Thievery from that
 jerk, the Master Assassin, take one step backwards.  Good choice,
 <playername>, because I was going to send you anyway.  My other thugs
 are too valuable to me.
 "Here's the deal.  I want the Master Key of Thievery, the Master Assassin
 has the Master Key of Thievery.  You are going to get the Master Key of Thievery
 and bring it back to me.  So simple an assignment even you can understand
 it."

==== Encouragement ====

If you subsequently chat to your quest leader, you are 'encouraged':

 "You don't seem to understand, 
 the Master Key of Thievery isn't here so neither should you be!"
 "May Kos curse you with lead fingers.  Get going!"
 "We don't have all year.  GET GOING!"
 "How would you like a scar necklace?  I'm just the jeweler to do it!"
 "Lazy S.O.B.  Maybe I should call up someone else..."
 "Maybe I should open your skull and see if my instructions are inside?"
 "This is not a task you can complete in the afterlife, you know."
 "Inside every living person is a dead person trying to get out,
 and I have your key!"
 "We're almost out of hell-hound chow, so why don't you just get moving!"
 "You know, the Master Key of Thievery isn't going to come when you
 whistle.  You must get it yourself."

=== Locate and goal levels ===

When first entering the locate level:
 Those damn little hairs tell you that you are nearer to
 the Master Key of Thievery.

When returning:
 Not wanting to face the Master of Thieves without having stolen
 the Master Key of Thievery, you continue.

When first entering the goal level:
 You feel a great swelling up of courage, sensing the presence of
 the Master Key of Thievery.  Or is it fear?

When returning:
 The hairs on the back of your neck whisper -- it's fear.

=== Quest nemesis ===

When first encountering the quest nemesis:

 "Ah!  You must be the Master of Thieves' ... er, `hero'.  A pleasure
 to meet you."

Upon further meetings:

 "We meet again.  Please reconsider your actions."

And on the 4th and subsequent meetings:

 "Surely, <playername>, you have learned that you cannot trust any bargains
 that the Master of Thieves has made.  I can show you how to continue on
 your quest without having to run into him again."

When you have the Master Key of Thievery, but the Master Assassin is still alive:

 "Please, think for a moment about what you are doing.  Do you truly
 believe that Kos would want the Master of Thieves to have
 the Master Key of Thievery?"

==== Discouragement ====

The Master Assassin will occasionally utter 'maledictions':

 "May I suggest a compromise.  Are you interested in gold or gems?"
 "Please don't force me to kill you."
 "Grim times are upon us all.  Will you not see reason?"
 "I knew the Master of Thieves, and you're no Master of Thieves, thankfully."
 "It is a shame that we are not meeting under more pleasant circumstances."
 "I was once like you are now, <playername>.  Believe in me -- our way
 is better."
 "Stay with me, and I will make you the Master Key of Thievery's guardian."
 "When you return, with or without the Master Key of Thievery,
 the Master of Thieves will have you killed."
 "Do not be fooled; I am prepared to kill to defend the Master Key of Thievery."
 "I can reunite you with the Twain.  Oh, the stories you can swap."

=== Victory ===

When picking up your quest artifact:

 As you pick up the Master Key of Thievery, the hairs on the back of your
 neck fall out.  At once you realize why the Master Assassin was
 willing to die to keep it out of the Master of Thieves' hands.  Somehow
 you know that you must do likewise.

When killing the nemesis:

 "I know what you are thinking, <playername>.  It is not too late for you
 to use the Master Key of Thievery wisely.  For the sake of your guild
 <brothers|sisters>, do what is right."
 You sit and wait for death to come for the Master Assassin, and then you
 brace yourself for your next meeting with the Master of Thieves!

When returning to your quest leader:

 "Well, I'll be damned.  You got it.  I am proud of you, a fine <currentrank>
 you've turned out to be.
 "While you were gone I got to thinking, you and the Master Key of Thievery
 together could bring me more treasure than either of you apart, so why don't
 you take it with you.  All I ask is a cut of whatever loot you come by.
 That is a better deal than I offered the Master Assassin.
 "But, you see what happened to the Master Assassin when he refused.
 Don't make me find another to send after you this time."

When subsequently throwing the Master Key of Thievery to the Master of Thieves:

 The Master of Thieves seems tempted to swap the Master Key of Thievery for
 the mundane one you detect in his pocket, but noticing your alertness,
 evidently chickens out.
 "Go filch the Amulet before someone else beats you to it.
 The Dungeons of Doom are back the way you came, through the magic portal."

=== Post-quest ===

When talking to the Master of Thieves after the quest:

 "Quite the little thief, aren't we, <playername>.  Can I interest you in a
 swap for the Master Key of Thievery.  Look around, anything in the keep
 is yours for the asking."

When talking to the Master of Thieves after getting the [[Amulet of Yendor|Amulet]]:
 "I see that with your abilities, and my brains, we could rule this world.
 "All that we would need to be all-powerful is for you to take that little
 trinket you've got there up to the Astral plane.  From there, Kos will
 show you what to do with it.  Once that's done, we will be invincible!"

{{DOD}}
----
[[职业任务分类]]
