'''药水'''是一种[[魔法]]的饮料。它们的[[重量]]是20并用感叹号!表示。它们有时可能很有用，但有时却是致命的。药水的效果可以通过喝掉它们或[[投掷]]它们去镶在某人的脑袋上来获得。一些药水可以相互混合来形成新的药水，这叫做[['''炼金术''']]([[alchemy]])


= 药水列表 =

||名字||[[价格]]||[[重量]]||概率/1000||[[魔法物品]]||[[外观]]||
||[[水]](未被诅咒的)||5||20||69||否||纯净||
||[[果汁]]||50||20||42||否||随机||
||[[看见隐形药水]]||50||20||42||是||随机||
||[[致病药水]]||50||20||42||否||随机||
||[[酒]]||50||20||42||否||随机||
||[[圣水]]||100||20||11.5||是||纯净||
||[[秽水]]||100||20||11.5||是||纯净||
||[[幻觉药水]]||100||20||40||是||随机||
||[[昏睡药水]]||100||20||42||是||随机||
||[[治疗药水]]||100||20||57||是||随机||
||[[恢复能力药水]]||100||20||40||是||随机||
||[[混乱药水]]||100||20||42||是||随机||
||[[额外治疗药水]]||100||20||47||是||随机||
||[[怪物探测药水]]||150||20||40||是||随机||
||[[物品探测药水]]||150||20||42||是||随机||
||[[致盲药水]]||150||20||40||是||随机||
||[[获得能量药水]]||150||20||42||是||随机||
||[[隐形药水]]||150||20||40||是||随机||
||[[加速药水]]||200||20||42||是||随机||
||[[完全治疗药水]]||200||20||10||是||随机||
||[[启蒙药水]]||200||20||20||是||随机||
||[[变形药水]]||200||20||10||是||随机||
||[[漂浮药水]]||200||20||42||是||随机||
||[[油]]||250||20||30||否||随机||
||[[酸]]||250||20||10||否||随机||
||[[获得能力药水]]||300||20||42||是||随机||
||[[获得等级药水]]||300||20||20||是||随机||
||[[麻痹药水]]||300||20||42||是||随机||

表格通过价格来排序，具体参考'''[[价格鉴定]]'''


= 产生 =

药水占有在主地牢随机生成的物品的16%，在容器中的18%，在[[Rogue层]]22%，在[[葛汉诺姆]]([[Gehennom]])的1%.出现的药水有1/8是[[被诅咒的]]，3/4是[[未被诅咒的]]，1/8是[[被祝福的]]。

在随机产生的基础上，药水可以在商店，尸骨([[bones]])文件，以及最近死去的[[怪物]]身上找到。

= 鉴定 =

除了总是以'''纯净'''的外观出现的水，药水有着下表中随机的外观。

{{{

  红宝石色(ruby)       粉色(pink)      橙色(orange)   黄色(yellow) 翡翠色(emerald)
   暗绿色(dark green)  青色(cyan)     天蓝色(sky blue)      亮蓝色(brilliant blue)  品红色(magenta)   紫红色(purple-red)    深褐色(puce)      奶白色(milky)       旋涡状(swirly )         泡沫状(bubbly)
   烟雾缭绕的(smoky )    浑浊的(cloudy)    沸腾的(effervescent)  黑色(black)          金色 (golden)
  棕色(brown)     起泡的(fizzy)       黑色(dark)         白色 (white)     朦胧的( murky)

}}}

如果你在游戏开局时带着[[油灯]]，那么[[油]]对你来说就是已鉴定的。通过'''[[价格鉴定]]'''它们很有效果。如果对药水的任何操作鉴定了药水，它们会立刻显出真名或令你命名([[#name]])它们。当然，你仍可以独立的这么做。


== 饮用 ==

先对药水进行[[诅咒鉴定|BUC鉴定]]。饮用被诅咒的药水是不明智的，尽管你能由此鉴定这瓶药水，但是你会错过有用药水的更多好处，并承受有害药水的更多负面效果。

除了[[麻痹药水]]([[potion of paralysis]])和[[变形药水]]([[potion of polymorph]])，所有随机药水的负面效果都是可以承受的(也有方法使这两瓶药水的影响变得可以承受)。这使得在大多数情况下通过饮用来鉴定药水是安全的，但即使这样，大多数药水还是需要通过它显示的提示和效果被鉴定并[[命名]](#name)。



[[麻痹药水|麻痹]]药水([[potion of paralysis]])和[[睡眠药水]]([[potion of sleeping]])会令你无法移动或陷入沉睡一些回合，不过效果会在50回合([[turn]]s)内消失。最严重的问题是怪物会在你不能运动的时候杀死你。避免这种危险的最安全的方式是戴上[[自由行动戒指]]([[ring of free action]])，它可以防止你进入麻痹或睡眠状态。其他的减免危险的方式包括停留在[[地牢]]的浅层，杀死你周围所有的怪物，并使你的宠物([[pet]]s)在你旁边。如果你具有强迫症的话，把自己锁在[[密室]](closet)<<用钥匙把一个房间的门都锁上。 __Msajka)>>里也是个不错的选择。

在你喝药水之前保证你的[[生命值]]([[health]])是满的。这样，当你喝下[[治疗药水]]([[potion of healing]])或[[额外治疗药水]]([[potion of extra healing]])时，你的最大生命值或能量(即魔法值，power)会得到增长。另外，这样当你喝下[[酸]]([[potion of acid]])或[[致病药水]]([[potion of sickness]])时你死的可能性会降低。
