[[盗贼]]任务要求你打倒[[刺客大师]]([[Master Assassin]])拿到[[盗贼大师钥匙]]([[The Master Key of Thievery]]). 如果想知道更多关于职业任务[[分支]]的信息, 可以查看[[职业任务]]条目.
<<TableOfContents>>
== 各层地图 ==

这个职业任务的地图中按照下面的概率生成随机怪物:

*96/175	(55%)	[[拉布列康]]([[leprechaun]])

*24/175	(14%)	随机[[宁芙|n]]

*24/175	(14%)	[[守卫娜迦]]([[guardian naga]])

*6/175	(3%)	随机[[娜迦|N]]

*1/7	(14%)	通常随机怪物

=== 盗贼工会大厅 ===

x=[[盗贼大师]]([[Master of Thieves]])

{{attachment:rogue_quest_1_guild_hall.png}}

通往[[末日地牢]]([[Dungeons of Doom]])<<FootNote(不要问我为什么是这么中二的名字, 问[[DevTeam]]去.--F_XZ)>>的双向传送门在地图左上部分[[陷阱|^]]标记的位置. [[盗贼大师]]([[Master of Thieves]])在标记着'x'的位置.
在那里同时有一个[[宝箱]].
和[[盗贼大师]]在同一个房间里的还有两个[[恶棍]], 旁边紧邻着相通的两个房间里面分别还有3个和4个[[恶棍]].
图中标记着的4个下楼的[[楼梯]]中的3个实际上是[[拟形怪]],
每个"梯子"的旁边都有一个[[宁芙]]和一个[[拉布列康]].
这层里面另外随机散布着5只[[水宁芙]]和4只[[拉布列康]], 还有8只[[变色龙]]和16个随机的[[陷阱]].<<FootNote(盗贼的老巢里面就全是陷阱和小偷这不是常识吗.--F_XZ)>>
The two-way [[magic portal]] back to the Dungeons of Doom is at the marked point. The [[Master of Thieves]] is at the point marked 'x', where there is also a [[chest]]; two [[thug]]s are in the room with him, and three and four more in the two rooms immediately connecting to his. Three of the four marked downstairs are actually [[mimic]]s; each of the four also has a water nymph and leprechaun next to it, and there are five more water nymphs and four more leprechauns distributed fairly evenly about the streets; there are also eight [[chameleon]]s on the level, and sixteen random [[trap]]s.
The entire level is [[no-teleport]] and has [[undiggable]] walls and floor.

=== 上填充层 ===

上填充层只是由房间和走廊组成的"普通"的一层. 这层会有6个房间, 5只[[拉布列康]], 2只[[水宁芙]], 以及两只[[守卫娜迦]],
地上会有9个随机物品还有8个随机[[陷阱]].
This is an "ordinary" room-and-corridor level, with six rooms; five leprechauns, two water nymphs, and two guardian nagas; nine random objects; and eight random traps.

=== 刺客工会大厅 ===

{{attachment:rogue_quest_2_assasin_guild_hall.png}}

[[楼梯]]会在这层随机的位置. 另外这层会随机分布18只[[拉布列康]], 6只[[守卫娜迦]], 5只[[变色龙]], 还有3只随机[[娜迦|N]];
6个随机[[陷阱]]; 还有15个随机物品.
在左下角标记着[[卷轴|?]]的位置有一个诅咒的[[传送卷轴]].
这整层的墙都是挖不动的但是传送是允许的.
The stairs are located randomly on the level, as are eighteen leprechauns, six guardian nagas, five chameleons, and three random N; six random traps; and fifteen random objects. There is a cursed [[scroll of teleportation]] at the marked spot in the lower left.
The entire level has undiggable walls. Teleportation is permitted.

=== 下填充层 ===

和上填充层一样,
下填充层也是类似普通地牢层的由房间和走廊构成的.
这层同样有6个房间, 5只[[拉布列康]], 2只[[水宁芙]], 和2只[[守卫娜迦]]; 9个随机物品; 还有8个随机陷阱.
These are "ordinary" room-and-corridor levels, with six rooms; five leprechauns, two water nymphs, and two guardian nagas; nine random objects; and eight random traps.

=== 刺客大师的据点 ===

{{attachment:rogue_quest_3_stronghold.png}}

通往这层的楼梯在这一层左边部分随机的某个房间里.
[[刺客大师]]拿着[[序幕铃铛]]([[Bell of Opening]])和[[盗贼大师钥匙]]在图中标记着x的位置.
长得像$形状的水池里有4只[[鲨鱼]].
在标记着%的位置有一个变色龙肉罐头.
这层里随机分布着8只[[拉布列康]],
8只[[守卫娜迦]], 5只[[变色龙]]以及3只随机的[[娜迦|N]];
13个随机物品; 还有11个随机陷阱, 除此之外在标记着^的位置还有个[[刺坑]]([[spiked pit]]).
The stairway into the level is randomly placed in one of the rooms on the left side of the level. The [[Master Assassin]], with the [[Bell of Opening]] and the [[Master Key of Thievery]], is at the point marked [[Master Assassin]]. The $-shaped pool is stocked with four [[shark]]s. There is a [[tin]] of chameleon meat at the marked spot <<Color2(%,cyan)>> in a left-central room. Randomly placed around the level are eighteen leprechauns, eight guardian nagas, five chameleons, and three random N; thirteen random objects; and eleven random traps, in addition to the one marked spiked pit.

这整层禁止传送并且墙也无法挖开.
注意这一层有4个分开的无法连通的区域.
下到这一层的楼梯在最左边的部分,
这一部分的边界在地图上用红色标记出来了,
并且和刺客大师所在的区域是分开的;
这意味着你如果想要碰上任务目标敌人并且成功完成职业任务就得要么靠[[相位移动]]穿墙或者从上一层[[落|洞]]下来碰运气, 有可能你就随机落到了正确的区域.
右边两个区域一个里面有$形状的水池.
如果你选择落到这个区域请先确保你有如果没有落到正确的区域的时候能够回到上一层然后重新尝试的办法.
你有大约1/4的机会落到正确的区域并且就算你进了这里你也需要一个可以[[层间传送]]([[level teleport]])或者[[相位移动]]的办法离开这个区域(回去交差).
The entire level is no-teleport and has undiggable walls. Note that this level has four separate, unconnected sections.  The stairs down to the level are in the leftmost section, whose boundary is marked in red.  This area is cut off from the Master Assassin; you can only reach the quest nemesis and successfully complete this quest by either [[phasing]] through these walls, or [[hole|falling]] from the level above and hoping that you'll randomly fall into the correct section.  The other two sections are the $-shaped pool and the rightmost section.  If you fall, make sure you have a means of return in case it's necessary to try repeatedly!  You have about a one in four chance falling into the correct section, and even if you are successful in entering it, you will need a way to [[level teleport]] or [[phasing|phase]] out of that section.

除此之外, [[地震鼓]]([[drum of earchquake]])也有办法吵醒boss.<<FootNote(据nethackwiki说[http://tasvideos.org/GameResources/DOS/Nethack.html Exploiting] [[bug#C343-426|bug C343-426]]这个bug也可以让你穿墙, 不过NAO上很有可能已经修复, 利用bug也会让你的成绩蒙灰.)>>
Also, a [[drum of earthquake]] can summon the nemesis. ([http://tasvideos.org/GameResources/DOS/Nethack.html Exploiting] [[bug#C343-426|bug C343-426]] could get you across the wall, too.)

有时候用[[魔法哨子]]可以把一个足够高等级的宠物带到中间区域.
让宠物随机走到刺客大师的附近可能会花一些时间不过这迟早可以做到. (把宠物送进去之后就一直站在最左边区域的右下角一直等到宠物打到了刺客大师就可以.)
Sometimes, a [[magic whistle]] can be used to bring a sufficiently high level pet into the area. It takes some time for the pet to wander near the Master Assassin, but will eventually happen. (After the pet is sent in, stand at the bottom right of the accessible area until it engages the Master Assassin.)

这一层是被承认的精心设计了的. 盗贼就应该是足智多谋的!
This is a deliberate and acknowledged design decision; Rogues are expected to be resourceful!

== Messages ==
=== Entry ===
First time: 
 Unexpectedly, you find yourself back in Ransmannsby, where you trained to
 be a thief.  Quickly you make the guild sign, hoping that you AND word
 of your arrival reach the Master of Thieves' den.

Next time:
 Once again, you find yourself back in Ransmannsby.  Fond memories are
 replaced by fear, knowing that the Master of Thieves is waiting for you.

If already rejected twice due to bad alignment:
 You rub your hands through your hair, hoping that the little ones on
 the back of your neck stay down, and prepare yourself for your meeting
 with the Master of Thieves.

=== Quest guardians ===

If #chatting before the quest is complete:
 "I hear that Lady Tyvefelle's household is lightly guarded."
 "You're back?  Even the Twain don't come back anymore."
 "Can you spare an old cutpurse a zorkmid for some grog?"
 "Fritz tried to join the other side, and now he's hell-hound chow."
 "Be careful what you steal, I hear the boss has perfected turning
 rocks into worthless pieces of glass."

If #chatting after the quest is complete:
 "I was sure wrong about Lady Tyvefelle's house; I barely got away with my
 life and lost my lock pick in the process."
 "You're back?  Even the Twain don't come back anymore."
 "Can you spare an old cutpurse a zorkmid for some grog?"
 "Fritz tried to join the other side, and now he's hell-hound chow."
 "Be careful what you steal, I hear the boss has perfected turning
 rocks into worthless pieces of glass."

=== Quest leader ===

When you first meet your quest leader:
 "Well, look who it is boys -- <playername> has come home.  You seem to have
 fallen behind in your dues.  I should kill you as an example to these
 other worthless cutpurses, but I have a better plan.  If you are ready
 maybe you could work off your back dues by performing a little job for
 me.  Let us just see if you are ready..."

When you return, having been rejected due to lack of experience:
 "Well, I didn't expect to see you back.  It shows that you are either stupid,
 or you are finally ready to accept my offer.  Let us hope for your sake it
 isn't stupidity that brings you back."

This message is not currently used:
 "Did you perhaps mistake me for some other Master of Thieves?  You must
 think me as stupid as your behavior.  I warn you not to try my patience."

When you are expelled from the quest for having failed the alignment test
seven times:
 "Well thugs, it looks like our friend has forgotten who is the boss
 around here.  Our friend seems to think that <currentranks> have been put in
 charge.  Wrong.  DEAD WRONG!"
 Your sudden shift in surroundings prevents you from hearing the end
 of the Master of Thieves' curse.

When being rejected due to lack of experience:
 "In the time that you've been gone you've only been able to master the
 arts of a <currentrank>?  I've trained ten times again as many Robbers
 in that time.  Maybe I should send one of them, no?  Where would that
 leave you, <playername>?  Oh yeah, I remember, I was going to kill you!"

When being rejected due to having worse than pious alignment:
 "Maybe I should chain you to my perch here for a while.  Perhaps watching
 real chaotic men at work will bring some sense back to you.  I don't
 think I could stand the sight of you for that long though.  Come back
 when you can be trusted to act properly."

When finally assigned the quest:
 "Will everyone not going to retrieve the Master Key of Thievery from that
 jerk, the Master Assassin, take one step backwards.  Good choice,
 <playername>, because I was going to send you anyway.  My other thugs
 are too valuable to me.
 "Here's the deal.  I want the Master Key of Thievery, the Master Assassin
 has the Master Key of Thievery.  You are going to get the Master Key of Thievery
 and bring it back to me.  So simple an assignment even you can understand
 it."

==== Encouragement ====

If you subsequently chat to your quest leader, you are 'encouraged':

 "You don't seem to understand, 
 the Master Key of Thievery isn't here so neither should you be!"
 "May Kos curse you with lead fingers.  Get going!"
 "We don't have all year.  GET GOING!"
 "How would you like a scar necklace?  I'm just the jeweler to do it!"
 "Lazy S.O.B.  Maybe I should call up someone else..."
 "Maybe I should open your skull and see if my instructions are inside?"
 "This is not a task you can complete in the afterlife, you know."
 "Inside every living person is a dead person trying to get out,
 and I have your key!"
 "We're almost out of hell-hound chow, so why don't you just get moving!"
 "You know, the Master Key of Thievery isn't going to come when you
 whistle.  You must get it yourself."

=== Locate and goal levels ===

When first entering the locate level:
 Those damn little hairs tell you that you are nearer to
 the Master Key of Thievery.

When returning:
 Not wanting to face the Master of Thieves without having stolen
 the Master Key of Thievery, you continue.

When first entering the goal level:
 You feel a great swelling up of courage, sensing the presence of
 the Master Key of Thievery.  Or is it fear?

When returning:
 The hairs on the back of your neck whisper -- it's fear.

=== Quest nemesis ===

When first encountering the quest nemesis:

 "Ah!  You must be the Master of Thieves' ... er, `hero'.  A pleasure
 to meet you."

Upon further meetings:

 "We meet again.  Please reconsider your actions."

And on the 4th and subsequent meetings:

 "Surely, <playername>, you have learned that you cannot trust any bargains
 that the Master of Thieves has made.  I can show you how to continue on
 your quest without having to run into him again."

When you have the Master Key of Thievery, but the Master Assassin is still alive:

 "Please, think for a moment about what you are doing.  Do you truly
 believe that Kos would want the Master of Thieves to have
 the Master Key of Thievery?"

==== Discouragement ====

The Master Assassin will occasionally utter 'maledictions':

 "May I suggest a compromise.  Are you interested in gold or gems?"
 "Please don't force me to kill you."
 "Grim times are upon us all.  Will you not see reason?"
 "I knew the Master of Thieves, and you're no Master of Thieves, thankfully."
 "It is a shame that we are not meeting under more pleasant circumstances."
 "I was once like you are now, <playername>.  Believe in me -- our way
 is better."
 "Stay with me, and I will make you the Master Key of Thievery's guardian."
 "When you return, with or without the Master Key of Thievery,
 the Master of Thieves will have you killed."
 "Do not be fooled; I am prepared to kill to defend the Master Key of Thievery."
 "I can reunite you with the Twain.  Oh, the stories you can swap."

=== Victory ===

When picking up your quest artifact:

 As you pick up the Master Key of Thievery, the hairs on the back of your
 neck fall out.  At once you realize why the Master Assassin was
 willing to die to keep it out of the Master of Thieves' hands.  Somehow
 you know that you must do likewise.

When killing the nemesis:

 "I know what you are thinking, <playername>.  It is not too late for you
 to use the Master Key of Thievery wisely.  For the sake of your guild
 <brothers|sisters>, do what is right."
 You sit and wait for death to come for the Master Assassin, and then you
 brace yourself for your next meeting with the Master of Thieves!

When returning to your quest leader:

 "Well, I'll be damned.  You got it.  I am proud of you, a fine <currentrank>
 you've turned out to be.
 "While you were gone I got to thinking, you and the Master Key of Thievery
 together could bring me more treasure than either of you apart, so why don't
 you take it with you.  All I ask is a cut of whatever loot you come by.
 That is a better deal than I offered the Master Assassin.
 "But, you see what happened to the Master Assassin when he refused.
 Don't make me find another to send after you this time."

When subsequently throwing the Master Key of Thievery to the Master of Thieves:

 The Master of Thieves seems tempted to swap the Master Key of Thievery for
 the mundane one you detect in his pocket, but noticing your alertness,
 evidently chickens out.
 "Go filch the Amulet before someone else beats you to it.
 The Dungeons of Doom are back the way you came, through the magic portal."

=== Post-quest ===

When talking to the Master of Thieves after the quest:

 "Quite the little thief, aren't we, <playername>.  Can I interest you in a
 swap for the Master Key of Thievery.  Look around, anything in the keep
 is yours for the asking."

When talking to the Master of Thieves after getting the [[Amulet of Yendor|Amulet]]:
 "I see that with your abilities, and my brains, we could rule this world.
 "All that we would need to be all-powerful is for you to take that little
 trinket you've got there up to the Astral plane.  From there, Kos will
 show you what to do with it.  Once that's done, we will be invincible!"

{{DOD}}
----
[[职业任务]]
