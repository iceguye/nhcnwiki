'''鉴定'''(Identification)被认作是NetHack游戏的核心之一。大多数的物品在游戏开始时都是未被鉴定的，一般只描述了它的外观。游戏中有很多方法来鉴定它们。

<<TableOfContents>>

= 鉴定的机制 =
NetHack中有三个鉴定的“级别”：

 *自动鉴定

像正常所认知的鉴定一样，自动鉴定使物品的名字为其原本的名字，而不论它们的外观和你称呼它的名字。

 *提示鉴定

在一些情况下，你会被提示输入你刚刚使用的物品的名字。比如说怪物投掷的药水镶在了你的脑袋上，药水是不会被自动鉴定的。由怪物使用的产生不同效果的药水也会这样，比如说被诅咒的升级药水。把戒指扔进水槽有时也会产生这样的提示。

 *无提示鉴定

在一些情况下，有些物品效果可以使得翻看攻略或十分有经验的玩家可以凭经验鉴定他们。这在用魔杖刻写或用价格鉴定时很常见。你应该使用#name指令来为那些发生特定效果的物品起一个恰当的名字。

有时执行动作会消耗掉物品，此时你应该先命名那个将要消失的物品，然后当你再次得到它时则重新命名。一个常见的例子就是向独角兽(unicorn)投掷宝石(gems)：独角兽会“感激的”(gratefully)接受有价值的宝石，“彬彬有礼的”(graciously)接受玻璃。先命名以区分宝石意味着你再找到相同宝石时就知道了它的价值。如果独角兽只是彬彬有礼的接受了宝石，就重命名它为玻璃。把物品浸入(#dip)到药水里来鉴定也是同样的方法。

= 直接鉴定 =

这些方法不基于物品的特殊属性，都是通用方法，并且所有的结果由游戏引擎来命名（不需要玩家命名）。

== 初始知识 ==
你开局时携带的物品是自动被鉴定的。这对巫师来说十分有利，巫师开局时带有两本法术书，三张卷轴，两个戒指和一把法杖。根据你所选的角色，你还会鉴定一些武器或装甲。另外，如果你开局携带了油灯，油也会被自动鉴定。

== 魔法鉴定 ==

 *卷轴鉴定
当你阅读鉴定卷轴的时候，根据卷轴的祝福状态你会被允许选择一个或几个物品来进行鉴定。如下的表格说明了这些情况。
||祝福状态||	鉴定1 个||2个|| 3个||4个||所有||
||祝福的	||1/5(注)||1/5||1/5	||1/5||1/5||
||未被诅咒的	||21/25||1/25||1/25||1/25||1/25||
||被诅咒的||	总是||从不||从不||从不||从不||

注：如果你的运气是正数，你会鉴定两个物品。

 *鉴定法术

主要文章：鉴定术法术书

使用这种预测法术和阅读祝福的鉴定卷轴具有相同的效果，不过正数的运气不会使有时的鉴定数目从一个变成两个。

 *坐王座
坐王座的一种效果和阅读祝福的鉴定卷轴相同。
== 试金石 ==

主要文章：试金石(Touchstone)

通过在祝福的试金石之上摩擦(#rub)可以鉴定所有的有价值的宝石，无价值的玻璃和一些灰色石头。侏儒(gnome)或考古学家也可以用未被诅咒的试金石进行鉴定。不过它不能确定宝石或石头的BUC状态。

== 在商店出售 ==

在商店出售或购买物品会让店主告诉你物品的外观。如果它的外观是独特的且不是随机的，例如大多数武器或盔甲，他就会帮你鉴定出物品的类别。这只在进行禅<<FootNote(在盲目的状态下进行整局游戏，所谓色即是空，空即是色。)>>(Zen)的时候有用（又如探索魔杖，探测物品药水）。

= 间接鉴定 =

以下的鉴定方法需要用到一些知识（来源于攻略或者你自己的经验）。不过它们有的缺乏普遍性，有的只是显示一些含糊不清的信息。

== 价格鉴定 ==

主要文章：价格鉴定

尽管在商店里店主给你开出的价格基于你的魅力(charisma )和一些随机因素，但是你仍可以通过价格初步鉴定物品。这对于鉴定戒指，药水和魔杖及之后使用其他方法进行后续鉴定十分有效（尤其是游戏前期或“文盲”挑战<<FootNote(在不阅读或书写任何卷轴，法术书，不刻写任何文字的情况下完成整局游戏。)>>）。价格鉴定最简单和实用的用途是鉴定出鉴定卷轴（游戏中最便宜的卷轴）。

== 重量鉴定 ==

用重量鉴定一件未知物品不是太有效，不过仍能提供一些线索。尤其是那些可能被诅咒的又很麻烦的装备，比如说浮空靴的重量是15。另外一个例子是盒子里的灰色石头可能会是负重石。<<FootNote(捡起灰色宝石前先踢上一脚，没踢动就说明是负重石不要捡了。)>>


使用重量鉴定的方法是通过用石头和金币使自己刚好负重，之后捡起待测物品，再一个个放下金币或石头直到解除负重，数一数放下的金币或石头的个数，由此可换算出物品的重量 。<<FootNote(其中金币的重量是0.01，石头的重量是10 - - Msajka)>>

== 行为鉴定 ==

 *被动鉴定

指发现了物品的特性但是并不能自动鉴定出物品的名字。例如对着龙使用驱魔魔杖(wand of cancellation)会听见龙在咳嗽，不过游戏仍会保持它的随机外观。

与被动鉴定相反则是正式鉴定(formal identification).

 *使用鉴定

戒指，药水，卷轴以及装甲都可以通过这种方式来鉴定。一些物品会在你穿上或使用的那一瞬间就可以被鉴定，比如说大多数药水，速度之靴，精灵斗篷等等；一些物品则需要一些时间来发挥特性，例如缓慢消化戒指；一些物品会导致一些不平常的事情发生而被鉴定，例如各种抗性戒指，反射护身符，魔法呼吸护身符；你也可以利用启蒙（即启蒙药水，启蒙魔杖及其他相同效果，可以获知玩家当前属性）来鉴定一些提供内禀属性的物品。

鉴定物品的时候一定要'''小心'''。
 *先通过宠物或祭坛来鉴定物品的祝福属性。
 *先使用独角兽角来鉴定药水（把独角兽角浸入到变形药水里会把它变形成其他的工具）
 *处于混乱状态或身穿强力的装甲时不要阅读卷轴（混乱时多产生负面效果，破环装备卷轴会玩坏你）

能够通过这种方式被鉴定的物品有：
 *斗篷(cloaks)- 除了魔法抵抗斗篷和隐形斗篷（对于已经隐形的玩家），所有的魔法斗篷在你穿上时都会被自动鉴定。这样，非隐形状态的玩家就可以鉴定出所有类型的斗篷。
 *靴子 - 笨拙之靴(fumble boot)和浮空之靴大多数都是被诅咒的。精灵靴（对于不能潜行的玩家）和速度之靴在穿戴时会被自动鉴定。弹跳之靴很好鉴定（试着跳一下）。如果你在穿上一双未被诅咒的靴子后什么也没发生也不能跳，你可能穿着踢踏之靴(kicking boots)或水上行走之靴，如果你可以施法的话就可以分辨它们，因为踢踏之靴会阻止你施法。
 *口哨 -吹口哨可以鉴定它。如果你吹的是魔法口哨且当前层有你的宠物，系统会自动鉴定它。
 
 *怪物使用鉴定

人形怪物会使用一些物品，这可以帮助我们鉴定。

 *他们有时会佩戴复生护身符，当他们死而复生的时候护身符就会被自动鉴定。
 *他们会饮用有益的药水（回复，大回复，全回复，速度，隐形，获得等级，变形），并投掷有害的药水（酸，盲目，混乱，睡眠，麻痹）。除了混乱药水，药水都可以通过被怪物饮用或投掷来自动鉴定。这是因为投掷酒和混乱药水有着相同的效果（其实怪物并不投掷酒）。
 *他们会阅读卷轴（创造怪物，大地，火焰，传送）。如果怪物阅读了卷轴却没有什么明显效果，他很可能阅读了创造怪物卷轴，新产生的怪物在你的视野之外。
 *他们会向你挥动攻击性法杖，对自己挥动变形杖，加速杖，隐形杖或传送杖。这可以帮助区分祛魔杖，隐形杖和传送杖，这是刻写无法区分的。

 *卷轴信息鉴定

这部分旨在说明阅读卷轴后通过它们显示的信息来鉴定它们。这段文字附属于The NetHack Object Identification Spoiler （Nethack物品鉴定攻略），是卷轴攻略的摘要。

在进行这种鉴定时最好先鉴定一下物品的BUC状态，并且只鉴定祝福的或未被诅咒的卷轴。同时，不要处于混乱，盲目，幻觉状态。最好在之前便通过价格鉴定出了鉴定卷轴（最便宜），这样便可以直接鉴定你的绝大部分物品，否则，才使用以下的技术。

'''小心失忆卷轴(scroll of amnesia)。'''没什么可以阻止它抹除你25%的记忆。在早期游戏它并不是很麻烦，因为你已鉴定的物品很少，不过保险起见，请带上你已学习的法术书以防遗忘。后期游戏中，最好使用价格鉴定来鉴定出失忆卷轴。失忆卷轴比鉴定卷轴贵十倍。

你需要采取一些预防措施来尽可能的减轻读卷轴可能的不利后果。

 *不要站在你的宠物或和平的怪物旁边读卷轴（可能是火焰卷轴或大地卷轴）。
 *确认你所有的可燃物品都是安全的（放到包里或扔到两格之外以防火焰卷轴）。
 *被诅咒的装甲附魔卷轴(scroll of enchant armor)会诅咒你的装甲，所以读诅咒卷时酌情脱衣服。
 *如果你测试的卷轴不是被诅咒的，你可以带上一件你不在乎的头盔，来保护你的头（大地卷轴引起的石头会砸在你头上），同时还可以鉴定破坏装甲卷轴和装甲附魔卷轴。
 *被诅咒的武器附魔卷轴不会诅咒你的武器，但是你在测试被诅咒的卷轴时仍最好拿一把你不在意的武器。

另一种方式是在混乱(confused)的时候读卷轴，它通常会减弱有害卷轴的效果（'''混乱状态下读灭绝卷轴会死掉'''）。例如，混乱时读天罚卷轴不会连接大铁球和铁链。看看[[卷轴]]攻略来了解在你混乱时具体发生了什么（尤其是信息）。

你也可以尽力使在阅读卷轴时地牢层中同时存在食物，药水，金币和陷阱，这样食物探测卷轴和金币探测卷轴就有更多机会被鉴定出来。（只在你测试被诅咒的卷轴时才需要药水和陷阱。） 

一些卷轴阅读后会自己鉴定出来，一些在特定情况下才会，而一些从来不会。如果你的角色是新人（beginner,六级以下的法师和七级以下的其他职业），一些卷轴阅读后会产生信息

'''“你一时有种奇怪的感觉，然后便消失了。”'''<<BR>>'' "You have a strange feeling for a moment, then ipasses."''

 如果你的角色不是新手，你就可以通过阅读卷轴所产生的效果和信息鉴定所有的卷轴。
 *自动鉴定的卷轴：健忘卷轴，空白卷轴，充能卷轴，大地卷轴，火焰卷轴，灭绝卷轴，鉴定卷轴，魔法地图卷轴，天罚卷轴，臭云卷轴。
 *大多数时候自动鉴定的卷轴：创造怪物卷轴（新怪物可见时），金币探测卷轴（未被诅咒且当前层存在金币），光明卷轴（未盲目），传送卷轴（祝福的或传送位置距离原位置足够远）。
 *特定情况下自动鉴定的卷轴：毁坏装甲卷轴（正穿着装甲），装甲附魔卷轴（正穿着装甲），武器充能卷轴（未被诅咒且挥舞着武器），食物探测卷轴（当前层存在食物）。
 *从不自动鉴定的卷轴：迷惑怪物卷轴，移除诅咒卷轴，惊吓怪物卷轴，驯服卷轴。

阅读卷轴将得到以下的结果。除非另有声明，默认为卷轴未被诅咒，玩家不是新人（beginner）且未处于盲目、混乱以及幻觉状态。

||卷轴||效果||信息||
||失忆卷轴( [[scroll of amnesia|Amnesia]])||你会忘记一些事物（例如陷阱，地图，发现等等）||''' "Who was that Maud person anyway?"'''<<BR>>'' 到底谁是Maud。''<<BR>> '''"Thinking of Maud you forget everything else."'''<<BR>>''你心中除了Maud什么都记不清了。''<<FootNote(Maud是由Alfred Lord Tennyson所写的同名诗歌中的一个女性形象，诗歌描绘的是叙述者陷入对心上人Maud的思念而进入无意识状态的幻觉场景。请参见www.englishverse.com/poems/maud 以阅读原文。        -- [[Msajka]])>>||
||空白卷轴( [[scroll of blank paper|Blank paper]])|| 不可使用||''' "This scroll seems to be blank."'''<<BR>>''这卷轴看起来是空白的。''||
|| 充能卷轴([[scroll of charging|Charging]])||选择一样物品来充能 || '''"Your <ring> spins clockwise for a moment."'''<<BR>>''你的戒指顺时针旋转了一会。''<<BR>>'''"Your <item> glows (blue|white) for a moment."'''<<BR>>''你的物品闪了一会蓝光/白光。''||
||迷惑怪物卷轴( [[scroll of confuse monster|Confuse monster]])||你将使你击中的下一个（或几个）怪物混乱。||'''"Your <hands> glow a brilliant red." '''<<BR>>''你的手闪烁着强烈的红光。 ''（被祝福的）<<BR>>'''"Your <hands> begin to glow red."'''<<BR>>''你的手闪烁着红光。''||
||制造怪物卷轴( [[scroll of create monster|Create monster]])||在你周围制造1到17个怪物||无||
||毁坏装甲卷轴( [[scroll of destroy armor|Destroy armor]])||如果当时你未穿着装甲，你的力量和体质会流失||''' "You have a strange feeling for a moment, then it passes."'''<<BR>> ''你一时有种奇怪的感觉，然后便消失了。''（未穿装甲，[[新人]]）<<BR>>''' "Your skin itches." '''<<BR>>''你的皮肤有些痒。''（未穿装甲，非[[新人]]）<<BR>>如果你穿着装甲就会出现直接描述你的装甲如何被毁坏的信息。||
||大地卷轴( [[scroll of earth|Earth]])||制造巨石。（混乱时阅读在你周围或头上制造2～6块石头） || '''"The ceiling rumbles (above|around) you."'''<<BR>>''在你（头上/周围）的天花板隆隆的响。''||
||装甲附魔卷轴( [[scroll of enchant armor|Enchant armor]])|| 若未穿装甲时阅读会锻炼（诅咒则流失）力量和体质。<<BR>>若穿着装甲将会移除装甲的诅咒（或祝福），增加装甲的附魔等级，将龙鳞转换成龙鳞铠甲。<<BR>>或令装甲蒸发（如果装甲的附魔等级过高的话）|| ''' "Your skin glows then fades." '''<<BR>>''你的皮肤发出光芒然后褪去了。''（未穿装甲）<<BR>> '''"Your <armor> glows silver for a moment."'''<<BR>>''你的<装甲>一时闪着银光。''<<BR>>'''"Your <armor> suddenly vibrates unexpectedly."'''<<BR>>''你的<装甲>突然振动起来。'' （过度附魔）<<BR>>'''"Your <armor> violently glows silver for while, then evaporates."'''<<BR>>''你的装甲闪起剧烈的银光，一会便蒸发了。''（过度附魔）||
||武器附魔卷轴([[scroll of enchant weapon|Enchant weapon]])||若未持武器时阅读会锻炼（诅咒则流失）灵活度。<<BR>>若手持武器则去除武器的诅咒，将蠕虫齿转变为啸刃刀(crysknife)，增加武器的附魔等级。<<BR>>或令武器蒸发（过度附魔）。若阅读时处于混乱状态，则使你的武器防锈/防火或修复损伤。||''' "Your <hands> (twitch|itch)."'''<<BR>>''你的手（抽搐/痒）起来。（未持武器）''<<BR>>  '''"Your weapon violently glows blue for a while and then evaporates."'''<<BR>>''你的武器迸发出刺眼的蓝光，一会便化为烟雾了。（过度附魔）''<<BR>>''' "Your <weapon> glows blue for a moment."'''<<BR>>''你的武器一时闪着蓝光。''<<BR>> '''"Your <weapon> is covered by a (shimmering golden shield|mottled purple glow)!"'''<<BR>>''你的武器（笼罩着淡淡的金光/闪起斑驳的紫光）。（增加或移除武器抗性）''<<BR>> ''' "Your<weapon> looks as good as new!"'''<<BR>>''你的武器看起来像新的一样！''（修复）||
|| 火焰卷轴([[scroll of fire|Fire]])||对你和你附近的怪物造成伤害：<<BR>>祝福的(3-5), 未被诅咒的(2-b3), 诅咒的(1-2), 史莱姆会被烧的干干净净。||''' "The scroll erupts in a tower of flame."'''<<BR>>''卷轴爆发出一根火柱。'' <<BR>> '''"The scroll catches fire and you burn your <hands>.'''<<BR>>''卷轴着火了并烧到了你的手。（混乱）''<<BR>>'''"Oh, look, what a pretty fire in your <hands>."'''<<BR>>''看那，你手里的火苗真可爱。（混乱且抗火）''||
||食物探测卷轴（ [[scroll of food detection|Food detection]]）||探测食物或药水（混乱或被诅咒的）。<<BR>>给予你食物评估（Food Appraisal）的内禀属性。（祝福的）||  '''"Your <nose> twitches then starts to tingle."'''<<BR>>''你的鼻子抽搐起来然后有点痛。''（祝福的）<<BR>>''' "Your nose tingles and you smell (food|something)."'''<<BR>>''你的鼻子抽搐起来然后你闻到了（食物|其他物品）。''<<BR>> '''"Your nose twitches."'''<<BR>>''你的鼻子抽搐起来。''||
||灭绝卷轴（ [[scroll of genocide|Genocide]]）||灭绝一整类怪物。（祝福的）<<BR>>灭绝一种怪物。（未被诅咒的）<<BR>>产生怪物。（被诅咒的）<<BR>>灭绝你自己的种族。（混乱且未被诅咒）||'''"What (class of) monster do you want to genocide?" '''<<BR>>''你想要灭绝哪类（种）怪物？''<<BR>>''' "Wiped out all <role>"'''<<BR>>''抹除所有的<你的种族>。''||
||金币探测卷轴（ [[scroll of gold detection|Gold detection]]）||探测金币，金质物品（祝福的）或金魔像。<<BR>>如果阅读时处于混乱状态，探测陷阱（3.4.3及以下版本不包括振动方块） || '''"You feel very greedy, and sense gold." '''<<BR>>''你感到自己很贪婪，感受到了金币的味道。''<<BR>>''' "You feel entrapped."'''<<BR>>''你感觉自己落入陷阱。''||
|| 鉴定卷轴（[[scroll of identify|Identify]]）||鉴定你物品栏里的物品，混乱时读则无效果。|| '''"This is an identify scroll."'''<<BR>>''这是鉴定卷轴。''||
|| 光明卷轴（[[scroll of light|Light]]）||照亮你身边，除非它是诅咒的或你混乱、在肚子里、在旋风里、被吞噬。<<BR>>卷轴是诅咒的则会带来黑暗。 ||''' "A lit field surrounds you!"'''<<BR>>''一个光亮的场子环绕着你！''||
||魔法地图卷轴（ [[scroll of magic mapping|Magic mapping]]）|| 探测整个地牢层，包括密门（祝福的）|| '''"A map coalesces in your mind!" '''<<BR>>''你的脑海中组成了一幅地图！''<<BR>>''' "Your mind is filled with crazy lines!"'''<<BR>>''你的脑子里充满了乱糟糟的线！''（禁止魔法地图层）||
|| [[scroll of punishment|Punishment]]
| chained to a heavy iron ball and chain, or nothing (confused or blessed)
| "You are being punished for your misbehavior!" or (confused or blessed) "You feel guilty."
|-
| [[scroll of remove curse|Remove curse]]
| removes curses or punishment (blessed)
| "You feel like someone is helping you." or (confused) "You feel like you need some help."
|-
| [[scroll of scare monster|Scare monster]]
| similar to engraving Elbereth, only one to be affecting by simply picking up or dropping
| (read it, blessed or uncursed, monster affected) "You hear maniacal laughter close by." or (no creature affected) "You hear maniacal laughter in the distance." or(pick up)"The scroll turns to dust as you pick it up."
|-
| [[scroll of stinking cloud|Stinking cloud]]
| creates a poisonous gas cloud around a square you choose (great if you're poison-resistant). You can discharge it harmlessly by choosing a square a long way away.
| "You have found a scroll of stinking cloud!" and (no effect) "You smell rotten eggs."
|-
| [[scroll of taming|Taming]]
| tames (or angers, if cursed) monsters that are directly next to you (or within 11x11 sqare area, if confused). Also calms shopkeepers.
| possibly no message, or (cursed) "<Monster> gets angry!" Note: this scroll is identified by elimination, as the only scroll with no message or visible effect.
|-
| [[scroll of teleportation|Teleportation]]
| teleports you to another square on the level (or another level, if cursed or confused), possibly of your choosing (if blessed or you have control teleport intrinsic)
| possibly no message, or (non-teleport level) "A mysterious force prevents you from teleporting!", or (carrying the Amulet of Yendor) "You feel disoriented for a moment."
|}

When bad scrolls do good:
{|class="wikitable"
! Scroll !! Conditions !! Effect !! Message
|-
| Destroy armor, cursed
| confused, wearing armor
| <nowiki>Makes one random worn piece of armor (rust|fire)proof (but does not repair existing damage).</nowiki>
| <nowiki>"Your <armor> glows purple for a moment."</nowiki>
|-
| Fire
| surrounded by slime, or inside a monster
| The slime is burned away or causes damage to the monster.
| none
|}
----
[[策略类 ]]
